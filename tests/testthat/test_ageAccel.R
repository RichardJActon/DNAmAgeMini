context("ageAccel")

test_that("age acceleration is predicted age minus chonological age",{
	expect_true(
		all(
			ageAccel(
				c(50, 20), c(70, 10) # pred
			) == c(-20, 10) # cron
		)
	)
})

# error in dif lengths
#
