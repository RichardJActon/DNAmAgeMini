[![pipeline status](https://gitlab.com/RichardJActon/DNAmAgeMini/badges/master/pipeline.svg)](https://gitlab.com/RichardJActon/DNAmAgeMini/commits/master)

# DNAmAgeMini

__WARNING: THIS PACKAGE IS A PRE-RELEASE WORK IN PROGRESS__

An R package to Calculate DNA methylation Age using 'beta' values from Illumina methylation arrays with several models

- Currently available models
	- Horvath 2013 'multi-tissue'[^1]
	- Hannum et al. 2013 'blood'[^2]
	- Levine et al. 2018 'PhenoAge'[^3]
	- Horvath et al. 2018 'Skin and Blood'[^4]

__Note that at present this package simply computes the prediction from the model coefficients, Normalisation and Imputation steps are NOT preformed__

```mermaid
graph LR
	subgraph pre-processing
	A(Normalise) --> B(Impute)
	end
	subgraph This Package
	B --> C(Calculate DNAm Age)
	end
	style C fill:#ccf,stroke:#f66,stroke-width:2px,stroke-dasharray: 5, 5
```

DNA methylation age predictions produced are simply the output of a simple linear equation of the form:

```math
DNAmAge = a\beta_{probe\ 1} ... b\beta_{probe\ n} + intercept
```

Where $`a`$, $`b`$ and $`intercept`$ are model parmeters provided by the authors of the respective DNAm age predictors.

(The Hovath 2013[^1] and Horvath 2018[^4] model results are subject to an additional transformation as described in the original publication, this transformation __Is__ carried out by this package)

# Installation

```
devtools::install_gitlab("richardjacton/DNAmAgeMini")
```

# References

[^1]: Horvath, Steve. 2013. “DNA methylation age of human tissues and cell types.” Genome Biology 14 (10): R115. doi:[10.1186/gb-2013-14-10-r115.](https://doi.org/10.1186/gb-2013-14-10-r115)
[^2]: Hannum, Gregory, Justin Guinney, Ling Zhao, Li Zhang, Guy Hughes, SriniVas Sadda, Brandy Klotzle, et al. 2013. “Genome-wide Methylation Profiles Reveal Quantitative Views of Human Aging Rates.” Molecular Cell 49 (2). Elsevier Inc.: 359–67. doi:[10.1016/j.molcel.2012.10.016.](https://doi.org/10.1016/j.molcel.2012.10.016)
[^3]: Levine, Morgan E, Ake T Lu, Austin Quach, Brian H Chen, Themistocles L Assimes, Stefania Bandinelli, Lifang Hou, et al. 2018. “An epigenetic biomarker of aging for lifespan and healthspan.” Aging 10 (4): 573–91. doi:[10.18632/aging.101414.](https://doi.org/10.18632/aging.101414)
[^4]: Horvath, Steve, Junko Oshima, George M. Martin, Ake T. Lu, Austin Quach, Howard Cohen, Sarah Felton, et al. 2018. “Epigenetic clock for skin and blood cells applied to Hutchinson Gilford Progeria Syndrome and *ex vivo* studies.” Aging 10 (7): 1758–75. doi:[10.18632/aging.101508](https://doi.org/10.18632/aging.101508)
